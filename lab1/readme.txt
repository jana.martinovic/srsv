1. laboratorijska vježba - raskrižje (lakša inačica)

Simulacija raskrižja ostvarena je na način da je svaki element zasebna dretva. 
Elementi komuniciraju preko zajedničke memorije -> najčešće redovi, rjeđe liste i globalne varijable

Implementacijski detalji: 
    imamo 4 glavna aktora - UPR, SEM, korisnik i RAS. Svaki od njih ostvaren je dretvom i ima neke pomoćne dretve u sebi pa ih redom opisujem:

    1. UPR
        Upravljač treba slušati korisnike koji su došli na raskrižje i crveno im je (simulacija pritiska na gumb za zeleno)
        također treba upravljati ciklusima na semaforu.
        Te dvije funkcionalnosti ostvarene su pomoću dretvi upravljacka_dretva_korisnici() i upravljac_ciklusima():

            a) upravljacka_dretva_korisnici()
                prima poruke iz komunikacijskog kanala "korsnik_upravljac" 
                te po potrebi mijenja globalne varijable dugi_ciklus_vodoravni, dugi_ciklus_okomiti

            b)upravljac_ciklusima()
                na temelju istinitosti varijabli dugi_ciklus_okomiti/vodoravni javlja semaforu da započne novi ciklus
                za to koristi kom. kanal "upravljac_semafor", te čeka odgovor na kom. kanalu "semafor_upravljač" 

    2. SEM
        Semafor treba paliti i gasiti semafore ovisno o ciklusu u kojem se nalazi te odgovarati korisnicima na upit o svjetlu.
        To je također ostvareno pomoću dvije dretve:
        
            a) semafor_ciklus()
                od UPR-a prima informaciju o sljedećem ciklusu, izvodi ga, pa mu javlja kada je završio
            
            b) semafor_info()
                ostvaren pomoću jednog javnog kanala "korisnik_semafor" na kojem prima id korisnika i željeni smjer
                zatim otvara privatni kanal s tim korisnikom preko kojeg za vrijeme dok korisnik postoji komuniciraju o svjetlu
                privatni kanali ostvareni su pomoću dictionaryja
                prije nego korisnik nestane zapiše se u listu  "korisnik_semafor_brisanje"
                semafor periodički (ako je to moguće, dictionary se ne koristi u tom trenutku) izbriše sve korisnike koji su otišli
    3. KORISNIK 
        Dretva korisnik stvara nasumičnog korisnika (A ili P i nasumični smjer). Javlja da je došao svima koje se to tiče.
        Zatim svaku sekundu pita semafor je li zeleno (simulacija vida). Kad dođe zeleno simulira kretanje pomoću funkcija hodaj() ili vozi()
        pritom javlja raskrižju svoju poziciju kako bi ga mogao nacrtati

    4. RAS
        Raskrižje sluša informacije o svjetlima i o korisnicima i njihovim pozicijama.
        Iscrtava pozicije svih korisnika i ispisuje informaciju o svjetlu svaku sekundu.


    U glavnom dijelu programa nakon što započnemo dretve (UPR, SEM, RAS) stvaramo korisnike nasumično svakih 1-3 sekunde.
    Za simulaciju polupraznog/praznog raskrižja kako bi se pokazala funkcionalnost kratkog ciklusa odkomentira se zakomentirani kôd na kraju.

    I autima i pješacima treba 10s da prijeđu cijelo raskrižje. 
    Kratki ciklus traje 15s za automobile i 5s za pješake.
    Dugi ciklus traje 30s za automobile i 20s za pješake. 
    Između kratkog i dugog ciklusa bude "crveno za sve" 7s 
        da automobili koji su nagazili u raskrižje u zadnjoj sekundi ne gaze ne gaze pridošle korisnike iz suprotnog smjera.


Pokretanje programa i korištenje:

    Program se može pokrenuti iz komandne linije naredbom py simulacija_raskrizja.py
    Budući da su sve petlje beskonačne (zbog jednostavnosti) program se gasi prisilno. 
        

