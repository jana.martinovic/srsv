from threading import *
import threading
import time
import random
import queue

#konstante: nikad se ne mijenjaju
smjerovi_auto = ["S-J", "J-S", "I-Z", "Z-I"]
smjerovi_pjesak = ["SI-SZ", "SZ-SI", "SI-JI", "JI-SI", "JI-JZ", "JZ-JI", "JZ-SZ", "SZ-JZ"]
okomiti_smjer = ["S-J", "J-S", "SZ-JZ", "JZ-SZ", "SI-JI", "JI-SI"]
vodoravni_smjer = ["Z-I", "I-Z", "SZ-SI", "SI-SZ", "JZ-JI", "JI-JZ"]


#globalne varijable (zajednicka memorija) pomocu kojih crtamo raskrizje
SA0 = SA1 = SA2 = SA3 = SB0 = SB1 = SB2 = SB3 = JA0 = JA1 = JA2 = JA3 = JB0 = JB1 = JB2 = JB3 = " "
SZSI = SISZ = SZJZ = SIJI = JZSZ = JISI = JZJI = JIJZ = " "
ZA3 = ZA2 = ZA1 = ZA0 = CSZ = CSI = IA0 = IA1 = IA2 = IA3 = " "
CA0 = CB0 = CAG = CBD = " "
ZB3 =ZB2 = ZB1 = ZB0 = CJZ = CJI = IB0 = IB1 = IB2 = IB3 = " "
upaljeni_semafori_info = ""



#sljedece funkcije su za rad sa semaforima
def upali_vodoravne():
    for semafor in semafori:
        if(semafor in vodoravni_smjer):
            semafori[semafor] = "zeleno"
    return
def ugasi_vodoravne_pjesacke():
    for semafor in semafori:
        if(semafor in vodoravni_smjer):
            if(len(semafor) == 5):
                semafori[semafor] = "crveno"
    return
def ugasi_vodoravne_aute():
    for semafor in semafori:
        if(semafor in vodoravni_smjer):
            if(len(semafor) == 3):
                semafori[semafor] = "crveno"
    return
def upali_okomite():
    for semafor in semafori:
        if(semafor in okomiti_smjer):
            semafori[semafor] = "zeleno"
    return
def ugasi_okomite_pjesacke():
    for semafor in semafori:
        if(semafor in okomiti_smjer):
            if(len(semafor) == 5):
                semafori[semafor] = "crveno"
    return
def ugasi_okomite_aute():
    for semafor in semafori:
        if(semafor in okomiti_smjer):
            if(len(semafor) == 3):
                semafori[semafor] = "crveno"
    return


#dretva i pomocne dretve koje ostvaruju semafor
def semafor_ciklus():
    global upaljeni_semafori_info, dugi_ciklus_vodoravni, dugi_ciklus_okomiti

    while(True):
        novi_smjer, trajanje = upravljac_semafor.get().split(",")

        if (trajanje == "kratko"):
            zeleno_svima = 5
            zeleno_samo_autima = 10
            upaljeni_semafori_info = "Kratki ciklus: "
        else:
            zeleno_svima = 20
            zeleno_samo_autima = 10
            upaljeni_semafori_info = "Dugi ciklus:   "

        if(novi_smjer == "vodoravni"):
            upali_vodoravne()
            dugi_ciklus_vodoravni = False
            upaljeni_semafori_info += "svi vodoravni imaju zeleno"
            time.sleep(zeleno_svima)
            ugasi_vodoravne_pjesacke()
            upaljeni_semafori_info = upaljeni_semafori_info[:15]
            upaljeni_semafori_info += "auti vodoravno imaju zeleno"

            time.sleep(zeleno_samo_autima)
            ugasi_vodoravne_aute()
            upaljeni_semafori_info = upaljeni_semafori_info[:15]
            upaljeni_semafori_info = "svima je crveno"
        else:
            upali_okomite()
            dugi_ciklus_okomiti = False
            upaljeni_semafori_info += "svi okomiti imaju zeleno"
            time.sleep(zeleno_svima)
            ugasi_okomite_pjesacke()
            upaljeni_semafori_info = upaljeni_semafori_info[:15]
            upaljeni_semafori_info += "auti okomito imaju zeleno"
            time.sleep(zeleno_samo_autima)
            ugasi_okomite_aute() 
            upaljeni_semafori_info = upaljeni_semafori_info[:15]
            upaljeni_semafori_info += "svima je crveno"
        
        semafor_upravljac.put("zavrsen")
def semafor_info():
    while(True):
        #čitaj zahtjeve (id-jeve)
        kljuc = korisnik_semafor.get()

        #za svaki zahtjev na javnom kanalu pošalji odgovor u privatnom kanalu
        for privatni_kanal in privatni_kanali:
            if(privatni_kanal == kljuc):
                id, smjer = kljuc.split(";")
                for semafor in semafori:
                    if (semafor == smjer):
                        privatni_kanali[privatni_kanal].put(semafori[semafor])

        #ako ima korisnika koje treba obrisati izbriši ih iz liste privatnih kanala
        while(korisnik_semafor_brisanje):
            for kljuc in korisnik_semafor_brisanje:
                try:
                    del privatni_kanali[kljuc]
                except:
                    pass
            korisnik_semafor_brisanje.remove(kljuc)
def SEM():
    pracenje_ciklusa = Thread(target=semafor_ciklus)
    pracenje_ciklusa.start()
    vid_pjesacima = Thread(target=semafor_info)
    vid_pjesacima.start()

#dretva i pomocne dretve koje ostvaruju upravljac
def upravljac_ciklusima():
    global dugi_ciklus_vodoravni, dugi_ciklus_okomiti
    smjer = "vodoravni"
    while(True):
        trajanje = "kratko"
        if(smjer == "vodoravni" and dugi_ciklus_vodoravni or smjer == "okomiti" and dugi_ciklus_okomiti):
            trajanje = "dugo"
        #zapocni ciklus
        upravljac_semafor.put(smjer+","+trajanje)
        #cekaj da zavrsi
        semafor_upravljac.get()

        #jedno vrijeme treba biti svima crveno 
        time.sleep(7)  
        #promijeni smjer
        if (smjer == "vodoravni"):
            smjer = "okomiti"
        else:
            smjer = "vodoravni"
def upravljacka_dretva_korisnici():
    global dugi_ciklus_vodoravni, dugi_ciklus_okomiti
    while(True):
        novi = korisnik_upravljac.get()
        if(novi in okomiti_smjer):
            dugi_ciklus_okomiti = True
        elif(novi in vodoravni_smjer):
            dugi_ciklus_vodoravni = True
def UPR():
    slusanje_korisnika = Thread(target=upravljacka_dretva_korisnici)
    slusanje_korisnika.start()
    upravljanje_ciklusima = Thread(target=upravljac_ciklusima)
    upravljanje_ciklusima.start()
    return

#dretva koja ostvaruje korisnika (aute i pjesake)
def hodaj(smjer):
    for i in range(0,5):
        pozicija('P', smjer, 1)
        time.sleep(1)
    for i in range(0,5):
        pozicija('P', smjer, 2)
        time.sleep(1)
    #pozicija('P', smjer, 3)
    return
def vozi(smjer):
    for i in range(0, 10):
        pozicija('P', smjer, i+1)
        time.sleep(1)
def korisnik():

    #stvori random korisnika
    korak = 0
    tip = random.choice(["A", "P"])  
    if(tip == "A"):
        smjer = random.choice(smjerovi_auto)
    else:
        smjer = random.choice(smjerovi_pjesak)

    #uspostavi privatni kanal sa semaforom
    kljuc = str(threading.get_ident()) + ";" + smjer
    privatni_kanali[kljuc] = queue.Queue()
    #zatrazi svjetlo na javom kanalu (kljuc-a)
    korisnik_semafor.put(kljuc)
    #čekaj svjetlo
    svjetlo = privatni_kanali[kljuc].get()

    #javi se upravljacu
    if(svjetlo == "crveno"):
        korisnik_upravljac.put(smjer)
    
    pozicija(tip, smjer, korak)
    time.sleep(1)

    #javi raskrizju tko si i gdje si

    #cekaj zeleno (od semafora)
    while(svjetlo == "crveno"):
        korisnik_semafor.put(kljuc)
        svjetlo = privatni_kanali[kljuc].get()
        pozicija(tip, smjer, korak)
        time.sleep(1)
    #kad dode zeleno javi semaforu da te makne iz liste privatnih kanala
    korisnik_semafor_brisanje.append(kljuc)

    if(tip == "A"):
        vozi(smjer)
    else:
        hodaj(smjer)

    zavrsene_dretve.put(threading.current_thread())
    return

#dretve koje ostvaruju iscrtavanje raskrizja
def pozicija(tip, smjer, korak):
    global SA0, SA1, SA2, SA3, SB0,  SB1, SB2, SB3, JA0, JA1, JA2, JA3, JB0, JB1, JB2, JB3, SZSI, SISZ, SZJZ, SIJI, JZSZ, JISI, JZJI, JIJZ
    global ZA3, ZA2, ZA1, ZA0, CSZ, CSI, IA0, IA1, IA2, IA3, CA0, CB0, CAG, CBD, ZB3, ZB2, ZB1, ZB0, CJZ, CJI, IB0, IB1, IB2, IB3
    if(smjer == "S-J"):
        if(korak == 0):
            SA3 = 'A'
        elif(korak == 1):
            SA2 = 'A'
        elif(korak == 2):
            SA1 = 'A'
        elif(korak == 3):
            SA0 = 'A'
        elif(korak == 4):
            CSZ = 'A'
        elif(korak == 5):
            CA0 = 'A'
        elif(korak == 6):
            CJZ = 'A'
        elif(korak == 7):
            JA0 = 'A'
        elif(korak == 8):
            JA1 = 'A'
        elif(korak == 9):
            JA2 = 'A'
        elif(korak == 10):
            JA3 = 'A'

    elif(smjer == "J-S"):
        if(korak == 0):
            JB3 = 'A'
        elif(korak == 1):
            JB2 = 'A'
        elif(korak == 2):
            JB1 = 'A'
        elif(korak == 3):
            JB0 = 'A'
        elif(korak == 4):
            CJI = 'A'
        elif(korak == 5):
            CB0 = 'A'
        elif(korak == 6):
            CSI = 'A'
        elif(korak == 7):
            SB0 = 'A'
        elif(korak == 8):
            SB1 = 'A'
        elif(korak == 9):
            SB2 = 'A'
        elif(korak == 10):
            SB3 = 'A'

    elif(smjer == "I-Z"):
        if(korak == 0):
            IA3 = 'A'
        elif(korak == 1):
            IA2 = 'A'
        elif(korak == 2):
            IA1 = 'A'
        elif(korak == 3):
            IA0 = 'A'
        elif(korak == 4):
            CSI = 'A'
        elif(korak == 5):
            CAG = 'A'
        elif(korak == 6):
            CSZ = 'A'
        elif(korak == 7):
            ZA0 = 'A'
        elif(korak == 8):
            ZA1 = 'A'
        elif(korak == 9):
            ZA2 = 'A'
        elif(korak == 10):
            ZA3 = 'A'

    elif(smjer == "Z-I"):
        if(korak == 0):
            ZB3 = 'A'
        elif(korak == 1):
            ZB2 = 'A'
        elif(korak == 2):
            ZB1 = 'A'
        elif(korak == 3):
            ZB0 = 'A'
        elif(korak == 4):
            CJZ = 'A'
        elif(korak == 5):
            CBD = 'A'
        elif(korak == 6):
            CJI = 'A'
        elif(korak == 7):
            IB0 = 'A'
        elif(korak == 8):
            IB1 = 'A'
        elif(korak == 9):
            IB2 = 'A'
        elif(korak == 10):
            IB3 = 'A'
    elif(smjer == "SI-SZ"):
        if(korak == 0):
            SISZ = 'P'
        if(korak == 1):
            SB2 = 'P'
        if (korak == 2):
            SA2 = 'P'
        #radi jednostavnosti, pjesak nestane cim zavrsi s cestom - ne zakoraci na plocnik preko
    elif(smjer == "SZ-SI"):
        if(korak == 0):
            SZSI = 'P'
        if(korak == 1):
            SA2 = 'P'
        if (korak == 2):
            SB2 = 'P'
    elif(smjer == "SI-JI"):
        if(korak == 0):
            SIJI = 'P'
        if(korak == 1):
            IA1 = 'P'
        if (korak == 2):
            IB1 = 'P'
    elif(smjer == "JI-SI"):
        if(korak == 0):
            JISI = 'P'
        if(korak == 1):
            IB1 = 'P'
        if (korak == 2):
            IA1 = 'P'
    elif(smjer == "JI-JZ"):
        if(korak == 0):
            JIJZ = 'P'
        if(korak == 1):
            JB2 = 'P'
        if (korak == 2):
            JA2 = 'P'
    elif(smjer == "JZ-JI"):
        if(korak == 0):
            JZJI = 'P'
        if(korak == 1):
            JA2 = 'P'
        if (korak == 2):
            JB2 = 'P'
    elif(smjer == "JZ-SZ"):
        if(korak == 0):
            JZSZ = 'P'
        if(korak == 1):
            ZB1 = 'P'
        if (korak == 2):
            ZA1 = 'P'
    elif(smjer == "SZ-JZ"):
        if(korak == 0):
            SZJZ = 'P'
        if(korak == 1):
            ZA2 = 'P'
        if (korak == 2):
            ZB2 = 'P'   
    
    return
def RAS():
    global SA0, SA1, SA2, SA3, SB0,  SB1, SB2, SB3, JA0, JA1, JA2, JA3, JB0, JB1, JB2, JB3, SZSI, SISZ, SZJZ, SIJI, JZSZ, JISI, JZJI, JIJZ
    global ZA3, ZA2, ZA1, ZA0, CSZ, CSI, IA0, IA1, IA2, IA3, CA0, CB0, CAG, CBD, ZB3, ZB2, ZB1, ZB0, CJZ, CJI, IB0, IB1, IB2, IB3
    global upaljeni_semafori_info
    while(True):
        print(
        upaljeni_semafori_info + '\n' +
        '            (sjever)                 \n' +
        '                                     \n' +
        '             |'+SA3+'|'+SB3+'|                   \n' +
        '            '+SZSI+'|'+SA2+'|'+SB2+'|'+SISZ+'                  \n' +
        '            '+SZJZ+'|'+SA1+'|'+SB1+'|'+SIJI+'                   \n' +
        '          ---+'+SA0+'|'+SB0+'+---                \n' +
        '          '+ZA3+ZA2+ZA1+ZA0+CSZ+CAG+CSI+IA0+IA1+IA2+IA3+'                        \n' +           
        '(zapad)   --- '+CA0+' '+CB0+' ---    (istok)     \n' +
        '          '+ZB3+ZB2+ZB1+ZB0+CJZ+CBD+CJI+IB0+IB1+IB2+IB3+'                           \n' +
        '          ---+'+JA0+'|'+JB0+'+---                \n' +
        '            '+JZSZ+'|'+JA1+'|'+JB1+'|'+JISI+'                   \n' +
        '            '+JZJI+'|'+JA2+'|'+JB2+'|'+JIJZ+'                   \n' +
        '             |'+JA3+'|'+JB3+'|                   \n' +
        '                                     \n' +
        '             (jug)                   \n'
        )

        SA0 = SA1 = SA2 = SA3 = SB0 = SB1 = SB2 = SB3 = JA0 = JA1 = JA2 = JA3 = JB0 = JB1 = JB2 = JB3 = " "
        SZSI = SISZ = SZJZ = SIJI = JZSZ = JISI = JZJI = JIJZ = " "
        ZA3 = ZA2 = ZA1 = ZA0 = CSZ = CSI = IA0 = IA1 = IA2 = IA3 = " "
        CA0 = CB0 = CAG = CBD = " "
        ZB3 =ZB2 = ZB1 = ZB0 = CJZ = CJI = IB0 = IB1 = IB2 = IB3 = " "
        time.sleep(1)

    return

#redovi, liste i rječnici -> ostvaruju komunikacijske kanale među dretvama
korisnik_upravljac = queue.Queue()
semafor_upravljac = queue.Queue()
upravljac_semafor = queue.Queue()
korisnik_semafor = queue.Queue()
korisnik_semafor_brisanje = []
semafor_korisnik = queue.Queue()
privatni_kanali = {}
dugi_ciklus_okomiti = dugi_ciklus_vodoravni = False
zavrsene_dretve = queue.Queue()
semafori = {
    "S-J": "crveno",
    "J-S": "crveno",
    "I-Z": "crveno",
    "Z-I": "crveno",
    "SI-SZ": "crveno",
    "SZ-SI": "crveno",
    "SI-JI": "crveno",
    "JI-SI": "crveno",
    "JI-JZ": "crveno",
    "JZ-JI": "crveno",
    "JZ-SZ": "crveno",
    "SZ-JZ": "crveno"
}

#pocetak glavnog dijela programa -> pokretanje dretvi UPR, SEM, RAS
upravljac = Thread(target=UPR, args=())
semafor = Thread(target=SEM)
raskrizje = Thread(target=RAS)
raskrizje.start()
semafor.start()
upravljac.start()
time.sleep(2)

#dok se program ne terminira slucajno generira korisnike raskrizja svakih 1-3 sekunde
while(True):
    t=Thread(target=korisnik, args=())
    t.start()
    time.sleep(random.randrange(1,3))

    #ako se ovaj odsjecak odkomentira nakon svakog korisnika
    #bacamo novcic i jednom u 20 puta će doći pauza od 80s
    #koja je dovoljna da se raskrizje rascisti i provjeri
    #radi li funkcionalnost "kratki ciklus ako nema cekajucih korisnika"
    """
    if(random.randrange(1,20) == 2):
        print("Pauza, nitko ne dolazi 80s, simulacija kratkog ciklusa")
        time.sleep(80)
    """

    while (not zavrsene_dretve.empty()):
        dretva = zavrsene_dretve.get()
        dretva.join()


