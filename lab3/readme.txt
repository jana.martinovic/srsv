3. laboratorijska vježba - lift (lakša inačica)

simulacija lifta ostvarena je pomoću 3 dretve: 
    - main dretva stvara putnike
    - dretva za ispisivanje trenutnog stanja
    - dretva koja simulira rad lifta

stvaranje putnika: 
    putnici se stvaraju na nasumičnom katu, s nasumičnim ciljem i s nasumičnim imenom (veliko slovo za smjer GORE i malo slovo za smjer DOLJE)
    
    učestalost stvaranja putnika i pauze (da se gužva rasčisti) podešavaju se namještanjem varijable "opterećenost"
        0-1: mala opterećenost - putnika ima puno manje nego što je limit od 10 po katu
        1-2: umjerena opterećenost - putnika ima relativno malo u početku ali se stvaraju brže nego što se lift kreće. Nastaju gužve.
        >3: velika opterećenost - putnika skoro stalno ima točno 10 na katu (max)

lift:
    kapacitet lifta podešava se promjenom varijable "kapacitet". Pretpostavljena vrijednost je 6.

    ako nema putnika lift stoji

    ako putnika ima lift se kreće na način:
        - ako ima barem jedna osoba na katu koja ide u istom smjeru kao lift, lift staje na tome katu i svi ulaze
        - lift nastavlja u svom smjeru dok ima ljudi iznutra koji idu u tom smjeru
        - lift mijenja smjer ako svi preostali ljudi u liftu idu u smjeru suprotnom od lifta
        - ako lift stoji, a ima ljudi koji čekaju, lift kreće u smjeru osobe koja je prva stisnula gumb
    
ispis:
    smjer može biti 'G' = gore, 'D' = dolje, 'N' = nema smjer
    vrata mogu biti 'O' = otvorena, 'Z' = zatvorena
    stajanja pokazuju katove na kojima netko čeka. 
        -*-- znači da netko čeka na 2. katu
        *-** znači da netko čeka na 1., 3., i 4. katu

    
