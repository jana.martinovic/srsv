import random
import time
import string
from threading import *

kapacitet = 6
second = 3

imena_gore = []
imena_dolje = []

zahtjevi = []


voznja = []
rijeseni = [[], [], [], [], []]

putnici = []
lista_od = []
lista_do = []


class Lift():
    def __init__(self):
        self.kat = 1
        self.vrata = 'Z'
        self.smjer = 'N'
        
    def pokreni(self):
        global voznja
        global zahtjevi

        while(True):
            while(voznja+zahtjevi):
                stani = False

                #ako je netko u liftu zatražio izlaz na ovom katu
                for vozac in voznja:
                    if(vozac.end == self.kat):
                        stani = True
                        break

                #ako postoji netko s ovog kata da želi u istom smjeru kao lift
                for zahtjev in zahtjevi:                
                    if(zahtjev.start == self.kat):
                        if(zahtjev.smjer == self.smjer):
                            stani = True

                #ako nema usputnih ali smo stigli do ovog koji je pozvao
                if(not stani and not voznja):
                    if(zahtjevi[0].start == self.kat):
                        stani = True

                if(stani):
                    self.stani()

                #penji/spuštaj se 1 kat
                if(self.smjer == 'G'):
                    time.sleep(second/2)
                    self.kat += 0.5
                    time.sleep(second/2)
                    self.kat += 0.5
                if(self.smjer == 'D'):
                    time.sleep(second/2)
                    self.kat -= 0.5
                    time.sleep(second/2)
                    self.kat -= 0.5
                if(self.smjer == 'N'):
                    if(not zahtjevi):
                        time.sleep(second/2)
                    else:
                        if(zahtjevi[0].start > lift.kat):
                            self.smjer = 'G'
                        else:
                            self.smjer = 'D'



        
    def stani(self):
        global voznja
        global zahtjevi
        self.vrata = 'O'

        time.sleep(second/2)

        #iskrcaj iz lifta one koji su stigli
        prebaceni = []
        for vozac in voznja:
            if(vozac.end == self.kat):
                rijeseni[int(self.kat)].append(vozac.ime)
                prebaceni.append(vozac)

        voznja = [x for x in voznja if x not in prebaceni]

        

        #dodaj nove s tog kata
        for zahtjev in zahtjevi:
            if(zahtjev.start == lift.kat and len(voznja) < kapacitet): #TODO and len(voznja)<kapacitet
                voznja.append(zahtjev)
        
        zahtjevi = [x for x in zahtjevi if x not in voznja]

        time.sleep(second/2)
        self.vrata = 'Z'

        #provjeri dal mijenjat smjer
        promjena = True
        for vozac in voznja:
            #ako u liftu ima nekog tko nastavlja u tom smjeru ne mijenjaj smjer
            if(vozac.smjer == lift.smjer):
                promjena = False
                break
        
        if(promjena):
            #ako je lift prazan
            if (len(voznja) == 0):
                self.smjer = 'N'

            #ako je u liftu bilo tko promijeni smjer
            else:
                self.smjer = voznja[0].smjer

    
    
class Putnik():
    def __init__(self):
        self.start, self.end = random.sample(range(1, 4+1), 2)
        self.ime = ""

        #generiraj nasumično ime za putnika
        if(self.start < self.end):
            self.smjer = 'G'
            if(imena_gore):
                self.ime = imena_gore.pop()
            else:
                imena_gore.extend(string.ascii_uppercase)
                random.shuffle(imena_gore)
                self.ime = imena_gore.pop()
        else:
            self.smjer = 'D'
            if(imena_dolje):
                self.ime = imena_dolje.pop()
            else:
                imena_dolje.extend(string.ascii_lowercase)
                random.shuffle(imena_dolje)
                self.ime = imena_dolje.pop()


        popunjenost = 0
        for zahtjev in zahtjevi:
            if(zahtjev.start == self.start):
                popunjenost += 1

        if(popunjenost < 10): 
            zahtjevi.append(self)
            putnici.append(self.ime)
            lista_od.append(str(self.start))
            lista_do.append(str(self.end))


def ispis():
    global lift
    while(True):
        kat = [[],[],[],[],[]]
        stajanja = ["-", "-", "-", "-"]
        for cekajuci in zahtjevi:
            kat[cekajuci.start].append(cekajuci.ime)
            stajanja[cekajuci.start-1] = "*"
        pozicija = ["", "", "", "", "", "", ""]
        for i in range(0, 7):
            if(i == (lift.kat-1)*2):
                rupa = "["
                for vozac in voznja:
                    rupa += vozac.ime
                rupa += " "*(kapacitet - len(voznja)) + "]"
                pozicija[i] = rupa
            else:
                pozicija[i] = " "+ " "*kapacitet + " "
        
        print('              Lift1    ')
        print('Smjer/vrata:    '+lift.smjer+' '+lift.vrata)
        print('Stajanja:=====  '+''.join(stajanja)+'  ==Izašli')
        praznih = " "*(10-len(kat[4]))
        print('4:' + ''.join(kat[4]) + praznih + '|'+pozicija[6]+'|'+ ''.join(rijeseni[4]))
        print('  ==========|'+pozicija[5]+'|')
        praznih = " "*(10-len(kat[3]))
        print('3:' + ''.join(kat[3]) + praznih + '|'+pozicija[4]+'|'+ ''.join(rijeseni[3]))
        print('  ==========|'+pozicija[3]+'|')
        praznih = " "*(10-len(kat[2]))
        print('2:' + ''.join(kat[2]) + praznih + '|'+pozicija[2]+'|'+ ''.join(rijeseni[2]))
        print('  ==========|'+pozicija[1]+'|')
        praznih = " "*(10-len(kat[1]))
        print('1:' + ''.join(kat[1])+ praznih + '|'+pozicija[0]+'|'+ ''.join(rijeseni[1]))
        print('=================================')
        print('Putnici: ' + ' '.join(putnici))
        print('     od: ' + ' '.join(lista_od))
        print('     do: ' + ' '.join(lista_do))

        
        print('\n\n\n')

        time.sleep(second/5)


lift = Lift()
ispisivanje = Thread(target = ispis, args=())
ispisivanje.start()
rad = Thread(target=lift.pokreni, args=() )
rad.start()

opterecenost = 0.5

while(True):
    Putnik()
    time.sleep(second/opterecenost)



