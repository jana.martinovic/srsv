#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mqueue.h>
#include <time.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <pthread.h>


// pamti zadnji id posla
struct zadnji_id{
	int id;
	pthread_mutex_t lock;
};

#define VELICINA sizeof(struct zadnji_id)

int broj_poslova;

//iz skripte
int shm_open (const char *name, int oflag, mode_t mode);
int shm_unlink (const char *name);
int ftruncate (int fildes, off_t length);
void *mmap (void *addr, size_t len, int prot, int flags, int fildes, off_t off);
int munmap (void *addr, size_t len);



int generiraj_posao(int pid, int k){
	mqd_t opisnik_reda;
	struct mq_attr attr;
	int descriptor; //opisnik dijeljene memorije
	int *poslovi; 
	char ime[20]; //ime spremnika

	//treba mi za for petlju
	srand(time(NULL) + pid);

	// poruka je "pid k ime" -> pid je max troznamenkast, k isto, ime ima 20 znakova i 2 razmaka
	char poruka[3+3+20+2];
	unsigned prioritet = 10;
	
	attr.mq_flags = 0;
	attr.mq_maxmsg = 5;
	attr.mq_msgsize = 20;

	//
	opisnik_reda = mq_open ("/msgq_example_name", O_WRONLY | O_CREAT, 00600, &attr);
	sprintf(ime, """/""%s-%d", getenv("SRSV_LAB5"), pid);
	descriptor = shm_open(ime, O_CREAT | O_RDWR, 00600);

	//todo obrada greske
	if (descriptor == -1 || ftruncate(descriptor, k) == -1){
		perror("shm_open/ftruncate");
		 return -1;
	}
	

	poslovi = mmap (NULL, k, PROT_READ | PROT_WRITE, MAP_SHARED, descriptor, 0);

	//todo obrada greske
	if (poslovi == (void *) -1) return -1;
	

	//generiraj poslove
	printf("G: posao %d %d %s [", pid, k, ime);
	int i = 0;
	for(i; i < k; i++){
		poslovi[i] = rand()%1000;
		printf (" %d ", poslovi[i]);
	}
	printf("]\n");

	
	if(pid + 1 == broj_poslova){
		munmap(poslovi, sizeof(21 * sizeof(int)));
		shm_unlink(ime);
	}


	//stvori poruku
	sprintf(poruka, "%d %d %s", pid, k, ime);

	//todo obrada greske
	if (opisnik_reda == (mqd_t) -1){ 
		return -1;
	}
	
	//šalji poruku + obrada greške
	if (mq_send (opisnik_reda, poruka, strlen (poruka) + 1, prioritet)) {
		return -1;
	}
	
	//ako ne radi ovdje onda stavi gore
	close(descriptor);

	return 0;
}



int main(int argc, char *argv[]){
	struct sched_param prio;

	struct zadnji_id *zadnji_id;
	int i = 0;
	int j = atoi(argv[1]); //broj poslova iz cmd
	broj_poslova = j;
	int k = atoi(argv[2]); //broj jedinica vremena iz cmd

 	//postavi prioritet
	prio.sched_priority = 50;
    	if (pthread_setschedparam(pthread_self(), SCHED_RR, &prio)) {
        	perror("sudo");
        	exit(1);
    	}

	char* ime_spremnika = getenv("SRSV_LAB5");

	int id = shm_open (ime_spremnika, O_CREAT | O_RDWR, 00600 );
			if (id == -1 || ftruncate ( id, sizeof(21 * sizeof(int))+k) == -1) {
				perror ("shm_open/ftruncate");
				exit(1);
		}

	//dohvati spremnik u kojem pise zadnji id
	zadnji_id =  mmap (NULL, VELICINA, PROT_READ | PROT_WRITE, MAP_SHARED, id, 0);
	if (zadnji_id == (void *) -1) {
				perror ( "mmap" );
				exit(1);
			}

	if(zadnji_id->id == 0){
		printf("%d\n", zadnji_id->id);
		zadnji_id-> id = 0;
		pthread_mutex_init(&zadnji_id->lock, NULL);
	}

	//preuzmi zadnji id, postavi novi
	pthread_mutex_lock(&zadnji_id->lock);
	int novi_id = zadnji_id->id;
	zadnji_id->id = novi_id + broj_poslova;
	pthread_mutex_unlock(&zadnji_id->lock);
	shm_unlink(zadnji_id);

	
	//generiraj j poslova
	for(i; i < j; i++){
		if (!fork()){
			srand(time(NULL)+i);
			generiraj_posao(i+novi_id,((rand()%k)+1));	
			exit (0);
		}
	}

	for(i = 0; i < j; i++)
		wait (NULL);
		
	return 0;
}



