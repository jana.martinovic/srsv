#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mqueue.h>
#include <time.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <mqueue.h>
#include <semaphore.h>
#include<time.h>
#include<pthread.h>

static sem_t sem_ceka, sem_radi;
int M;
int id_posla = 0;
static long iteracija_u_sekundi = 100;
char *poslovi[20];//inicijalizirati mem za svaki posao
int last_index;


int pthread_setschedparam(pthread_t thread, int policy, const struct sched_param *param);
int pthread_getschedparam(pthread_t thread, int *restrict policy, struct sched_param *restrict param);

void pronadi_sekundu(){
	long i = 0;
	double sekunda = 0;
    	struct timespec t1, t2;

    while (sekunda < 1.0) {
	iteracija_u_sekundi *= 2;
	//zapocni stopericu
	clock_gettime(CLOCK_MONOTONIC, &t1);
	//petlja
        for (i; i < iteracija_u_sekundi; i++)
            asm volatile ("":: :"memory");
	//zaustavi stopericu
	clock_gettime(CLOCK_MONOTONIC, &t2);
	long razlika_sec = t2.tv_sec - t1.tv_sec;
        sekunda += razlika_sec; 
    }
    iteracija_u_sekundi /= sekunda*2;
}

void spavaj(int broj){
	long i = 0;
	long ukupno_iteracija = iteracija_u_sekundi * broj;
	 for (i; i < ukupno_iteracija; i++) {
        asm volatile ("":: :"memory");
    }
}

void *radna(int *id_dretve) {
	
	//cekaj da te zaprima pusti
	 sem_wait(&sem_ceka);
	 while (1) {

		//ako nema poslova javi
		 if(poslovi[id_posla]==NULL){
		 	printf("R%d: nema poslova, spavam \n",*id_dretve);
			sem_wait(&sem_ceka);

			//optusti semafor za ostale dretve ako ova nema sta raditi
			sem_post(&sem_ceka);
			spavaj(1);	
		}

		//ako ima poslova
		else{
			sem_wait(&sem_radi);//postaviti ce ga i otpustiti dretva zaprima
			char posao[100];
			int trajanje_posla=0;
			char spremnik[40];
			int id;
			int *trajanja_svih_poslova;
		
			
			//uzmi podatke o poslu iz reda poslova, tu je ime spremnika *lab5sim neso
			sprintf(posao, "%s", poslovi[id_posla]);
			id_posla++;
			sem_post(&sem_radi);

			int id_procesa;
			sscanf(posao, "%d %d %s", &id_procesa, &trajanje_posla, spremnik);
	
			//uzmi posao iz spremnika
			id = shm_open (spremnik, O_CREAT | O_RDWR, 00600 );
			if ( id == -1 || ftruncate ( id, sizeof(21 * sizeof(int))+trajanje_posla ) == -1) {
				perror ( "shm_open/ftruncate" );
				exit(1);
			}
			
			//dohvati trajanja svih poslova
			trajanja_svih_poslova = mmap ( NULL, trajanje_posla, PROT_READ | PROT_WRITE, MAP_SHARED, id, 0 );
	
			if ( trajanja_svih_poslova == (void *) -1) {
				perror ( "mmap" );
				exit(1);
			}

			//obavi svaki posao
			for(int i=0 ; i< trajanje_posla; i++){
					printf ( "R id:%d obrada podatka: %d (%d/%d)\n",*id_dretve,trajanja_svih_poslova[i],i+1,trajanje_posla);
				
				spavaj(1);

			}	
			close ( id );
			
			shm_unlink(trajanja_svih_poslova);
				
		}	
	  
	 }	

}



 	





int main(int argc, char *argv[]){

	pronadi_sekundu();

	//napravi semafor
    sem_init(&sem_radi, 0, 1);//za radnu dretvu
    sem_init(&sem_ceka, 0, 4);

	int m = atoi(argv[1]);

	pthread_attr_t attr_radna;
	struct sched_param prio;

	//dretva zaprima, prioritet 60
	prio.sched_priority = 60;
    if (pthread_setschedparam(pthread_self(), SCHED_RR, &prio)) {
        exit(1);
    }
     

	    //dretva radna, prioritet 40
    pthread_attr_init(&attr_radna);
    pthread_attr_setinheritsched(&attr_radna, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setschedpolicy(&attr_radna, SCHED_RR);
    prio.sched_priority = 40;
    pthread_attr_setschedparam(&attr_radna, &prio);



 	//stvori m radnih dretvi
    	int *a;
    	a = malloc(m * sizeof(int)); //redni broj dretve
	
    pthread_t *t;
    t = malloc(m * sizeof(pthread_t)); //pokazivac na dretvu
    int i = 1;
    for (i; i < m; i++) {
	a[i] = i;
        pthread_create(&t[i],&attr_radna, radna, (void *) &a[i]); 
        }


    char poruka[20];
    int spavao_sekundi = 0; 
    int poruke = 0;
	size_t duljina;
	unsigned prioritet;


    mqd_t opisnik_reda;
    opisnik_reda = mq_open ("/msgq_example_name", O_RDONLY);
	if (opisnik_reda == (mqd_t) -1) {
		perror ("mq_open");
		return -1;
	}


   
	while(1){

        	//ako ima više od m poruka ili ako je prošlo više od 30 s
		if(poruke >= m || spavao_sekundi > 30){

            		//postavi oboje opet na 0
			poruke = 0;
            		spavao_sekundi = 0; 
			//dopusti dretvama da krenu radit
			sem_post(&sem_ceka); 
		}

		spavaj(1);
        	spavao_sekundi++; 

		duljina = mq_receive (opisnik_reda, poruka, 20, &prioritet );
		if (duljina < 0) {
            		//ako nema poruke idi dalje
			continue; 
		}
		
        	//to smijem samo ako radna ne koristi varijablu svi_poslovi, zato sem
		sem_wait(&sem_radi);

        	//dodaj posao: otkljucaj semafor, povecaj index, dodaj posao zatvori semafor
		poslovi[last_index] = malloc(strlen(poruka) + 1);
		strcpy(poslovi[last_index], poruka);
		last_index = last_index +1;
		sem_post(&sem_radi);
		
        	poruke++;
	}	

}


