lab 5 POSIX sučelje

posluzitelj.c i generator.c koji međusobno komuniciraju redovima poruka i zajedničkim spremnikom

generator: 

    prije pokretanja potrebno podesiti varijablu okoline. 
    Zajednicka naredba za sve: sudo SRSV_LAB5=lab5sim ./generator.exe [j] [k]
    argumenti:
        j = broj poslova 
        k = maksimalno vrijeme trajanja posla (trajanje je nasumičan broj između 0 i k)

    opis: 
        generator stvara j poslova (procesa koji obavljaju funkciju generiraj_posao)
        svaki proces ima ime (lab5sim-[redni_broj]) i šalje ga u red poruka 
        svaki taj proces stvori zajednički spremnik čije je ime (lab5sim-[redni_broj])
        u taj spremnik šalje podatke (nasumično generirane, onoliko podataka koliko je trajanje posla)

posluztelj:
    prije pokretanja potrebno podesiti varijablu okoline. 
    Zajednicka naredba za sve: sudo SRSV_LAB5=lab5sim ./posluzitelj.exe [m]
    argumenti:
        m = broj radnih dretvi

    opis:
        glavna dretva stvara m radnih dretvi
        zaprima poruke iz reda poruka (oblika lab5sim-[redni_broj]) i stavlja ih u FIFO red
        kada skupi >=m poruka radne dretve se pokreću

        radna dretva otvara spremnik (čije je ime oblika lab5sim-[redni_broj]) koji je preuzela iz FIFO reda
        simulira obavljanje posla
    
    
    
